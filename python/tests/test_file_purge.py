import datetime
import os
import tempfile

from unittest import TestCase

from freezegun import freeze_time

from backup_lib.file_purge import find_and_delete


def get_ts(year: int, month: int, day: int) -> float:
    """convenience function"""
    return datetime.datetime.combine(
        datetime.date(year=year, month=month, day=day),
        datetime.time.min,
    ).timestamp()


class TestFilePurge(TestCase):
    def setUp(self):
        self.temp_dir_obj = tempfile.TemporaryDirectory()
        self.temp_dir_name = self.temp_dir_obj.name

    def tearDown(self):
        self.temp_dir_obj.cleanup()

    def write_dated_file(self, filename: str, year: int, month: int, day: int) -> None:
        file_path = os.path.join(self.temp_dir_name, filename)
        file_ts = get_ts(year, month, day)

        with open(file_path, "a") as fd:
            fd.write("hi")

        os.utime(file_path, (file_ts, file_ts))

        return file_path

    @freeze_time("2023-09-05")
    def test_one_new(self):
        old_file = self.write_dated_file("old_file.txt", 2023, 8, 1)
        new_file = self.write_dated_file("new_file.txt", 2023, 9, 1)

        self.assertTrue(os.path.isfile(old_file))
        self.assertTrue(os.path.isfile(new_file))

        find_and_delete(
            pathname="*.txt",
            older_than_days=7,
            root_dir=self.temp_dir_name,
            recursive=True,
        )

        self.assertFalse(os.path.isfile(old_file))
        self.assertTrue(os.path.isfile(new_file))
