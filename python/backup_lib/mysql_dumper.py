import datetime
import itertools
import logging
import os
from dataclasses import dataclass
from subprocess import Popen, PIPE, run
from typing import Generator, Iterable

from backup_lib.file_purge import find_and_delete

IGNORE_DBS = {"", "mysql", "information_schema", "performance_schema"}


@dataclass
class DbNamePlusHost:
    name: str
    host: str


class DbDumper:
    def dump_database(
        self,
        database_host: str,
        database_name: str,
    ):
        # Thanks SO https://stackoverflow.com/a/17890393
        # pipes mysqldump output into gzip

        dbdump_cmd = [
            "mysqldump",
            "--host",
            database_host,
            "-u",
            self.db_user,
            "--add-drop-database",
            "--databases",
            database_name,
        ]

        db_bu_file = os.path.join(
            self.backup_dir,
            database_host,
            "mysql_{db_name}_{ts}.sql.gz".format(
                db_name=database_name,
                ts=datetime.datetime.now().strftime("%Y%m%dT%H%M"),
            ),
        )

        self.log.info(
            "Backing up db %s on %s to %s", database_name, database_host, db_bu_file
        )

        os.makedirs(os.path.dirname(db_bu_file), exist_ok=True)

        self.log.debug("Running to backup db: %s", " ".join(dbdump_cmd))
        with open(db_bu_file, "wb", 0) as bu_fd:
            p1 = Popen(dbdump_cmd, stdout=PIPE)
            p2 = Popen("gzip", stdin=p1.stdout, stdout=bu_fd)

        p1.stdout.close()  # force write error if p2 dies
        p2.wait()
        p1.wait()

    def list_databases(
        self, database_host: str
    ) -> Generator[DbNamePlusHost, None, None]:
        # mysql -u mysqldumpusr --host $HOSTNAME -e "show databases;" -sN

        cmd = [
            "mysql",
            "-u",
            self.db_user,
            "--host",
            database_host,
            "-e",
            "show databases;",
            "-sN",
        ]

        self.log.debug(
            "Running to list databases on %s: %s", database_host, " ".join(cmd)
        )

        db_name_out = run(cmd, text=True, capture_output=True)

        db_names = filter(
            lambda x: x not in IGNORE_DBS,
            db_name_out.stdout.split(os.linesep),
        )

        for db_name in db_names:
            yield DbNamePlusHost(name=db_name, host=database_host)

    def cleanup(self, retain_days: int):
        pattern = "**/mysql_*.sql.gz"

        find_and_delete(
            pattern,
            older_than_days=retain_days,
            root_dir=self.backup_dir,
            recursive=True,
        )

    def run(self):
        # dbs_to_backup = {x: self.list_databases(x) for x in self.db_hosts}

        dbs_to_backup = list(
            itertools.chain.from_iterable(map(self.list_databases, self.db_hosts))
        )

        for i in dbs_to_backup:
            self.dump_database(database_host=i.host, database_name=i.name)

    def __init__(
        self,
        backup_dir: str,
        db_hosts: Iterable[str],
        db_user: str,
        # db_pass: str
    ):
        self.log = logging.getLogger(__name__)
        self.backup_dir = backup_dir
        self.db_hosts = db_hosts
        self.db_user = db_user
