import datetime
import glob
import logging
import os

from typing import Optional

logger = logging.getLogger(__name__)


def get_file_age_days(filename):
    age_days = (
        datetime.datetime.now().timestamp() - os.path.getmtime(filename)
    ) / 86400
    logger.debug("Dating file %s, %s days", filename, age_days)
    return age_days


def find_and_delete(
    pathname: str, older_than_days: int, root_dir: str, recursive: Optional[bool] = True
):
    files = glob.iglob(pathname=pathname, root_dir=root_dir, recursive=recursive)
    files = map(lambda x: os.path.join(root_dir, x), files)
    files = filter(os.path.isfile, files)

    files = filter(
        lambda x: get_file_age_days(x) >= older_than_days,
        files,
    )

    for delete_file in files:
        logger.info("Deleting file %s", delete_file)
        os.remove(delete_file)
