#! /usr/bin/env python

import argparse
import logging
import os
import sys

from tempfile import gettempdir
from HealthCheck_Pinger import PingHC

from backup_lib.mysql_dumper import DbDumper

logger = logging.getLogger(__name__)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--host",
        type=str,
        dest="hosts",
        default=["localhost"],
        nargs="*",
        help="Host for database",
    )
    parser.add_argument(
        "-u", "--user", type=str, dest="user", default="root", help="database user"
    )
    parser.add_argument(
        "-d",
        "--directory",
        type=str,
        dest="directory",
        default=gettempdir(),
        help="Backup Directory",
    )

    parser.add_argument(
        "-r",
        "--retain-days",
        type=int,
        default=7,
        dest="retain_days",
        help="number of days to retain",
    )

    parser.add_argument(
        "-v", action="count", default=0, dest="verbosity", help="raise verbosity"
    )

    hc_grp = parser.add_argument_group("Healthchecks")

    hc_grp.add_argument(
        "--healthcheck-id",
        dest="hc_id",
        type=str,
        default="",
        help="uuid for healthchecks.io",
    )

    hc_grp.add_argument(
        "--healthcheck-server",
        dest="hc_srv",
        type=str,
        default="https://hc-ping.com",
        help="define an alternative server for healthcheck updates",
    )

    args = parser.parse_args()

    logging.basicConfig(
        format="%(levelname)s:%(message)s",
        level={0: logging.WARNING, 1: logging.INFO}.get(args.verbosity, logging.DEBUG),
    )

    logger.info(
        "Starting %s, backing up to %s", os.path.basename(__file__), args.directory
    )

    if args.hc_id:
        ping = PingHC(uuid=args.hc_id, server=args.hc_srv)
        ping_logger_txt = "Sending %s for job %s to %s"
        logger.debug(ping_logger_txt, "start", args.hc_id, args.hc_srv)

    if not os.getenv("MYSQL_PWD"):
        logger.error("Environment variable MYSQL_PWD is required.")
        if args.hc_id:
            logger.debug(ping_logger_txt, "failure", args.hc_id, args.hc_srv)
            ping.failure
        sys.exit(1)

    try:
        db_dumper = DbDumper(
            backup_dir=args.directory,
            db_hosts=args.hosts,
            db_user=args.user,
            # db_pass=db_pass,
        )
        db_dumper.cleanup(retain_days=args.retain_days)
        db_dumper.run()

        logger.info("Done!")
        if args.hc_id:
            logger.debug(ping_logger_txt, "success", args.hc_id, args.hc_srv)
            ping.success
    except Exception as e:
        logger.exception(e)
        if args.hc_id:
            logger.debug(ping_logger_txt, "failure", args.hc_id, args.hc_srv)
            ping.failure
        sys.exit(1)
