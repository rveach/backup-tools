from setuptools import setup
from setuptools.command.install import install


def readme():
    """print long description"""
    with open("README.md") as f:
        long_descrip = f.read()
    return long_descrip


setup(
    name="backup_tools",
    use_scm_version={
        "root": "..",
        "relative_to": __file__,
    },
    setup_requires=["setuptools_scm"],
    description="Tools to make backups easier",
    long_description=readme(),
    long_description_content_type="text/markdown",
    url="https://gitlab.com/rveach/backup-tools",
    author="Ryan Veach",
    author_email="rveach@gmail.com",
    license="MIT",
    classifiers=[
        "Development Status :: 4 - Beta",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 3",
    ],
    keywords=["backup"],
    packages=["backup_lib"],
    scripts=["scripts/mysql_backup.py"],
    install_requires=["HealthCheck-Pinger"],
    python_requires=">=3.8",
)
