FROM python:3.11-slim-bookworm

# this label is required to identify container with ofelia running
LABEL ofelia.service=true
LABEL ofelia.enabled=true

# https://github.com/mcuadros/ofelia
COPY --from=mcuadros/ofelia:latest /usr/bin/ofelia /usr/bin/ofelia

ARG PY_LIB_VER=setme

ENV PIP_ROOT_USER_ACTION=ignore
ENV PIP_DISABLE_PIP_VERSION_CHECK=1
ENV PIP_NO_CACHE_DIR=1

RUN apt-get -y update && apt-get install -y \
    default-mysql-client \
    musl-dev \
  && rm -rf /var/lib/apt/lists/*

# Ofelia was built in Alpine and is looking for musl libc.so
RUN ln -s /usr/lib/x86_64-linux-musl/libc.so /lib/libc.musl-x86_64.so.1

RUN pip install \
  "backup-tools==${PY_LIB_VER}" \
  --index-url https://gitlab.com/api/v4/projects/49067064/packages/pypi/simple

ENTRYPOINT ["/usr/bin/ofelia"]

CMD ["daemon", "--config", "/etc/ofelia/config.ini"]
